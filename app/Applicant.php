<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Applicants
 * @package APP
 *
 * @property $skills
 * @property $id
 * @property $name
 * @property $email
 * @property $website
 * @property $cover_letter
 * @property $job_id
 * @property $created_at
 * @property $updated_at
 */

class Applicant extends Model
{
    public $table = 'applicants';

    /**
     *  Defines that this model uses timestamps
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Skills()
    {
        return $this->hasMany(\App\Skill::class, 'applicant_id', 'id');
    }

}
