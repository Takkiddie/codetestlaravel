<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Job;
use Illuminate\Http\Request;

class MainController extends Controller
{

    /**
     * @var Job
     */
    protected $job;


    //To make one of these:
    //php artisan make:controller MainController --resource
    public function __construct(Job $job)
    {
        $this->job = $job;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = $this->job->get();
        $tableRows = $this->getRows($jobs);

        return view('index',compact('tableRows'));
    }

    /**
     * @param $someString
     * @param $rowspan
     * @param $show
     * @return string
     */
    function tdWrap($someString,$rowSpan = 1,$show = true) {
        $ret = '';
        if($show){
            $ret =  "<td rowspan=".$rowSpan." >".$someString."</td>";
        }
        return $ret;
    }

    function buildRow(&$job,&$applicant,&$skill,$showJob,$showApplicant) {
        $ret = '';
        $ret .= '<tr>';
        //Wrap in td. Set 'rowSpan' to a placeholder to replace later, once I have more data
        $ret .= $this->tdWrap($job->name,'$numJobSkills',$showJob);
        $ret .= $this->tdWrap($applicant->name,'$numApplicantSkills',$showApplicant);
        $ret .= $this->tdWrap($applicant->email,'$numApplicantSkills',$showApplicant);
        $ret .= $this->tdWrap($applicant->website,'$numApplicantSkills',$showApplicant);
        $ret .= $this->tdWrap($skill->name);
        $ret .= $this->tdWrap($applicant->cover_letter,'$numApplicantSkills',$showApplicant);
        $ret .= '</tr>';
        return $ret;
    }

    function getRows($jobs) {
        $ret = '';

        foreach ($jobs as $job) {
            $applicants = $job->Applicants;
            $numJobSkills = 0;  //Total Skills tied to all applicants in job.
            $showJob = true;    //Show Job on Current row.
            foreach ($applicants as $applicant) {
                $skills = $applicant->skills;
                $numApplicantSkills = 0;    //Total SKills tied to applicant
                $showApplicant = true;      //Show Applicant on Current row

                //Look at every skill tied to every applicant, tied to every job.
                foreach ($skills as $skill)
                {
                    //Build a row for that skill
                    $ret .= $this->buildRow($job,$applicant,$skill,$showJob,$showApplicant);
                    //Add skill to running total of all skills on applicant and job.
                    $numJobSkills += 1;
                    $numApplicantSkills += 1;
                    $showApplicant = false;
                    $showJob = false;

                }
                //Replace placeholder with actual number
                $ret = str_replace('$numApplicantSkills',$numApplicantSkills,$ret);
            }
            //Replace placeholder with number.
            $ret = str_replace('$numJobSkills',$numJobSkills,$ret);
        }
        return $ret;
    }

}
