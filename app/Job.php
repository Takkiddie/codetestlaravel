<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Applicants
 * @package APP
 *
 * @property $id
 * @property $name
 * @property $Applicant
 * @property $created_at
 * @property $updated_at
 */

class Job extends Model
{
    public $table = 'jobs';

    public function Applicants()
    {
        return $this->hasMany(\App\Applicant::class, 'job_id', 'id');
    }

}
