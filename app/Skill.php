<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Applicants
 * @package APP
 *
 * @property $id
 * @property $name
 * @property $applicant_id
 * @property $created_at
 * @property $updated_at
 */

class Skill extends Model
{
    public $table = 'skills';
}
