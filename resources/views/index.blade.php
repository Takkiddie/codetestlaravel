@extends('templates.master')

<?php

?>

@section('content')

<table class='job-applicants'>
    <tr>
        <th>Job</th>
        <th>Applicant Name</th>
        <th>Email Address</th>
        <th>Website</th>
        <th>Skills</th>
        <th>Cover Letter Paragraph</th>
    </tr>
    {!! $tableRows !!}
</table>

@endsection
