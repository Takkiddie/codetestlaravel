<div class="contains_all colorScheme">

<?php ?>
<link rel="stylesheet" href="/css/template/bootstrap.min.css">
<link rel="stylesheet" href="/css/main.css">

@include('partials.header')

    <div class ="container">
        @yield('content')
    </div>

<script type="text/javascript" src="js/template/jquery.min.js"></script>
<script type="text/javascript" src="js/template/bootstrap.min.js"></script>

@yield('scripts')


@include('partials.footer')

</div>
